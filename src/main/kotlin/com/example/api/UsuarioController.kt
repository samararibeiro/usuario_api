package com.example.api

import org.springframework.web.bind.annotation.GetMapping //pra fazer o get
import org.springframework.web.bind.annotation.PostMapping //pra fazer o post
import org.springframework.web.bind.annotation.PutMapping //pra fazer o put
import org.springframework.web.bind.annotation.DeleteMapping //pra fazer o delete
import org.springframework.web.bind.annotation.RequestParam //Para pegar o request param na api
import org.springframework.web.bind.annotation.RequestBody //Para pegar o request Body na api
import org.springframework.web.bind.annotation.PathVariable //Para pegar o path variable na api
import org.springframework.web.bind.annotation.RestController //controlador da api
import com.google.gson.Gson //bliblioteca para manipulação de json no kotlin
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

@RestController 
class UsuarioController {
    val gson = Gson() //instanciando o Gson

    val lista = mutableListOf("{id: 0, nome_usuario: 'Samara'}", "{id: 1, nome_usuario: 'Leticia'}");
    //mutableListOf = cria uma lista mutável que pode add ou remover elementos.

    @GetMapping("/usuario") //cria o endpoint get do usuario.
    //Criar uma função retornarUsuario que tem como parâmetro de request id e por default 0, id é int.
    //A função retorna um usuário.
    //requestParam é só o id. 
    fun retornarUsuario (@RequestParam(value = "id", defaultValue = "-1") id: Int): ResponseEntity<String> {
        
        //entra aqui se não passou um id
        if(id == -1) {
            //transformar a lista de string em uma só string.
            val listaEmTexto = lista.joinToString();
            return ResponseEntity (listaEmTexto, HttpStatus.OK)
        }
        
        lista.forEach {
            //gson é a biblioteca indicada na linha 11 e o fromJson é uma função dessa biblioteca.
            //que converte string para objeto. Recebe 2 parametros, string e o tipo do obj.
            //it é um item da lista tipo "id 0 e usuario Samara".
            val temp = gson.fromJson(it, Usuario::class.java);
            //o primeiro id é o requestparam o temp.id é da lista e significa que ele pega o objeto de temp e procura o id.
            if(id == temp.id){
                //gson.toJson passa de volta para string o objeto.
                return ResponseEntity (gson.toJson(temp), HttpStatus.OK)
            }
        };
        // se passar id inválido
        return ResponseEntity("Usuário não encontrado", HttpStatus.NOT_FOUND)
    }

    @PostMapping("/usuario")
    fun inserirUsuario(@RequestBody usuario: Usuario): ResponseEntity<String>{
        lista.forEach {
            val temp = gson.fromJson(it, Usuario::class.java);
            //se o nome ou id do usuario já existe
            if (usuario.id == temp.id || usuario.nome_usuario == temp.nome_usuario){
                return ResponseEntity("Usuário já existe", HttpStatus.PRECONDITION_FAILED)
            }
        }
        //se não existe, converte o usuário para string e add na lista
        lista.add(gson.toJson(usuario));
        return ResponseEntity("Usuário criado com sucesso", HttpStatus.CREATED)
    }

    @PutMapping("/usuario/{id}")
    //Colocar PathVariable Id: e identificar o tipo do ID
    fun atualizarUsuario(@PathVariable id: Int, @RequestBody usuario: Usuario): Response{
       //Para alterar a lista é necessário o index, já que o it no forEach comum é imutável.
        lista.forEachIndexed { index, it ->
            var temp = gson.fromJson(it, Usuario::class.java);
            //Se o objeto é encontrado
            if (temp.id == id) {
                //procura o index na lista e transforma em uma string e coloca na lista no index 
                //que deve ser atualizado.
                lista[index] = gson.toJson(usuario);
                return Response(204, "Usuário atualizado com sucesso");
            }
        }
        return Response(403, "Usuário não encontrado");
    }

    @DeleteMapping("/usuario/{id}")
    fun deletarUsuario(@PathVariable id: Int): Response {
        
        lista.forEachIndexed { index, it ->
            var temp = gson.fromJson(it, Usuario::class.java);
            if (temp.id == id) {
                //procura o index na lista e remove o item da lista
                lista.removeAt(index);
                return Response(200, "Usuário removido com sucesso");
            }
        }

        return Response(404, "Usuário não encontrado");
    }
}