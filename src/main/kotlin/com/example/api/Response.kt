package com.example.api

data class Response(val status: Int, val content: String)